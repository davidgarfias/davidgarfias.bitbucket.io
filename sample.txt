int TicketSystem_addWindow(TicketSystem * this, bool payment, bool procedure, bool service) {
  assert(this);

  if(this->windows == NULL) {
    this->windows = (Window **) malloc(sizeof(Window *));
    this->windowCount++;
  } else {
    void * tmp = realloc(this->windows, ++this->windowCount * sizeof(Window *));
    assert(tmp);
    this->windows = (Window **) tmp;
  }

  this->windows[this->windowCount - 1] = (Window *) malloc(sizeof(Window));
  assert(this->windows[this->windowCount - 1]);

  this->windows[this->windowCount - 1]->payment = payment;
  this->windows[this->windowCount - 1]->procedure = procedure;
  this->windows[this->windowCount - 1]->service = service;

  return (int) (this->windowCount - 1);
}

int TicketSystem_addTicket(TicketSystem * this, bool handicapped, TicketType type) {
  assert(this);

  Ticket * n = (Ticket *) malloc(sizeof(Ticket));

  if(n == NULL)
    return 0;

  DLL * queue;
  switch(type) {
    case payment:
      queue = this->paymentQueue;
      break;
    case procedure:
      queue = this->procedureQueue;
      break;
    case service:
      queue = this->serviceQueue;
  }

  bool success = false;
  int turn = 0;

  if(handicapped) {
    if(DLL_isEmpty(queue))
      success = DLL_insertFront(queue, (void *) n);
    else {
      DLL_cursorHead(queue);

      do {
        if( ((Ticket *) DLL_peek(queue))->handicapped == false)
          break;
      } while(DLL_cursorNext(queue));

      success = DLL_insertBefore(queue, (void *) n);
    }
  } else {
    success = DLL_insertBack(queue, (void *) n);
  }

  if(success) {
    n->number = (int) ++this->ticketCount;
    n->handicapped = handicapped;
    n->type = type;
    turn = (int) n->number;
  } else
    free(n);

  return turn;
}

int TicketSystem_windowNext(TicketSystem * this, size_t id) {
  assert(this);
  assert(this->ticketCount);
  assert(id < this->windowCount);

  int next = (int) this->ticketCount;
  TicketType next_type = payment;
  bool handicapped = false;

  bool a_payment = false, a_procedure = false, a_service = false;

  if(this->windows[id]->payment) {
    if(!DLL_isEmpty(this->paymentQueue))
      a_payment = true;
  }

  if(this->windows[id]->procedure) {
    if(!DLL_isEmpty(this->procedureQueue))
      a_procedure = true;
  }

  if(this->windows[id]->service) {
    if(!DLL_isEmpty(this->serviceQueue))
      a_service = true;
  }

  if(a_payment || a_procedure || a_service) {
    if(a_payment) {
      Ticket * tpay = (Ticket *) DLL_peekFront(this->paymentQueue);
      if(tpay->handicapped)
        handicapped = true;

      next = tpay->number;
    }

    if(a_procedure) {
      Ticket * tpro = (Ticket *) DLL_peekFront(this->procedureQueue);
      if(handicapped) {
        if(tpro->handicapped) {
          next_type = (tpro->number < next) ? procedure : next_type;
          next = (tpro->number < next) ? tpro->number : next;
        }
      } else {
        if(tpro->handicapped) {
          next_type = procedure;
          next = tpro->number;
          handicapped = true;
        } else {
          next_type = (tpro->number < next) ? procedure : next_type;
          next = (tpro->number < next) ? tpro->number : next;
        }
      }
    }

    if(a_service) {
      Ticket * tser = (Ticket *) DLL_peekFront(this->serviceQueue);
      if(handicapped) {
        if(tser->handicapped) {
          next_type = (tser->number) ? service : next_type;
          next = (tser->number < next) ? tser->number : next;
        }
      } else {
        if(tser->handicapped) {
          next_type = service;
          next = tser->number;
        } else {
          next_type = (tser->number < next) ? service : next_type;
          next = (tser->number < next) ? tser->number : next;
        }
      }
    }

    DLL * queue;
    switch(next_type) {
      case payment:
        queue = this->paymentQueue;
        break;
      case procedure:
        queue = this->procedureQueue;
        break;
      case service:
        queue = this->serviceQueue;
    }

    free(DLL_removeFront(queue));
  } else
    next = 0;

  return next;
}
